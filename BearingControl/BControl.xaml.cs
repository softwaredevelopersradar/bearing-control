﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Axes;
using Arction.Wpf.SemibindableCharting.EventMarkers;
using SpectrumPanoramaControl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace BearingControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class BControl : UserControl
    {
        #region Conctructors

        public delegate void SimpleDoubleEventHandler(double freq);
        public event SimpleDoubleEventHandler CursorOnFreq;

        public delegate void DoubleDoubleEventHandler(double startFreq, double endFreq);
        public event DoubleDoubleEventHandler RangeChangeEvent;

        public BControl()
        {
            InitializeComponent();

            ReadConfigSSR("SSR.json");

            InitStartParams();

            InitComponent();

            //CreatePoints();
        }

        public void SetLanguage(string param)
        {
            if (param.ToLower().Contains("ru"))
            {
                _MHz = "МГц";
                TranslateTextBlocks(_MHz);
                TranslateMarkers(_MHz);
            }
            if (param.ToLower().Contains("en"))
            {
                _MHz = "MHz";
                TranslateTextBlocks(_MHz);
                TranslateMarkers(_MHz);
            }
            if (param.ToLower().Contains("az"))
            {
                _MHz = "MHs";
                TranslateTextBlocks(_MHz);
                TranslateMarkers(_MHz);
            }
        }

        private void TranslateTextBlocks(string text)
        {
            MHzTextBlock.Text = text;
        }

        private void TranslateMarkers(string text)
        {
            for (int i = 0; i < sChart.ViewPolar.Markers.Count(); i++)
            {
                sChart.ViewPolar.Markers[i].Label.Text = text;
            }
        }

        private void CreatePoints()
        {
            int pointCount = 1000;
            Random random = new Random();
            SeriesPoint[] points = new SeriesPoint[pointCount];
            for (int i = 0; i < pointCount; i++)
            {
                points[i].X = random.NextDouble() * 1000.0;
                points[i].Y = random.NextDouble() * 360.0;
            }

            FFPLS.Points = points;
        }


        private void InitStartParams()
        {
            _GlobalNumberOfBands = parametrsOfSSR.Count;
            _GlobalRangeXmin = parametrsOfSSR.First().FminMHz;
            _GlobalRangeXmax = parametrsOfSSR.Last().FmaxMHz;
            _GlobalRangeYmax = 360;
            _GlobalRangeYmin = 0;
        }

        private void InitComponent()
        {
            //GenerateCustomTicks(ref xAxis);
        }

        #endregion

        #region Properties

        List<SSRParametrs> parametrsOfSSR = new List<SSRParametrs>();
        public List<SSRParametrs> ParametrsOfSSR
        {
            get { return parametrsOfSSR; }
        }

        private double _GlobalRangeXmin;
        public double GlobalRangeXmin
        {
            get { return _GlobalRangeXmin; }
            set
            {
                _GlobalRangeXmin = value;
                _ReCalc();
            }
        }

        private double _GlobalRangeXmax = 0;
        private double GlobalRangeXmax
        {
            get { return _GlobalRangeXmax; }
            set { _GlobalRangeXmax = value; }
        }

        private double _GlobalRangeYmin = 0;
        private double GlobalRangeYmin
        {
            get { return _GlobalRangeYmin; }
            set
            {
                _GlobalRangeYmin = value;
                SetYRange(_GlobalRangeYmin, _GlobalRangeYmax);
            }
        }

        private double _GlobalRangeYmax = 360;
        private double GlobalRangeYmax
        {
            get { return _GlobalRangeYmax; }
            set
            {
                _GlobalRangeYmax = value;
                SetYRange(_GlobalRangeYmin, _GlobalRangeYmax);
            }
        }

        private int _GlobalNumberOfBands;
        public int GlobalNumberOfBands
        {
            get { return _GlobalNumberOfBands; }
            set
            {
                _GlobalNumberOfBands = value;
                _ReCalc();
            }
        }

        private double _GlobalBandWidthMHz;
        public double GlobalBandWidthMHz
        {
            get { return _GlobalBandWidthMHz; }
            set
            {
                _GlobalBandWidthMHz = value;
                _ReCalc();
            }
        }

        private int _LiveTime = 3000;
        public int LiveTime
        {
            get { return _LiveTime; }
            set { _LiveTime = value; }
        }

        public double MinVisibleX
        {
            get { return xAxis.Minimum; }
            set
            {
                if ((value >= _GlobalRangeXmin) && (value < _GlobalRangeXmax) && (value < xAxis.Maximum))
                    xAxis.Minimum = value;
            }
        }
        public double MaxVisibleX
        {
            get { return xAxis.Maximum; }
            set
            {
                if ((value > _GlobalRangeXmin) && (value <= _GlobalRangeXmax) && (value > xAxis.Minimum))
                    xAxis.Maximum = value;
            }
        }
        private double MinVisibleY
        {
            get { return yAxis.Minimum; }
            set
            {
                if ((value >= _GlobalRangeYmin) && (value < _GlobalRangeYmax) && (value < yAxis.Maximum))
                    yAxis.Minimum = value;
            }
        }
        private double MaxVisibleY
        {
            get { return yAxis.Maximum; }
            set
            {
                if ((value > _GlobalRangeYmin) && (value <= _GlobalRangeYmax) && (value > yAxis.Minimum))
                    yAxis.Maximum = value;
            }
        }

        public string xAxisLabel
        {
            get { return xAxis.Title.Text; }
            set
            {
                xAxis.Title.Text = value;
            }
        }
        public string yAxisLabel
        {
            get { return yAxis.Title.Text; }
            set
            {
                yAxis.Title.Text = value;
            }
        }

        public bool yAxisLabelVisible
        {
            get { return yAxis.Title.Visible; }
            set
            {
                if (yAxis.Title.Visible != value)
                {
                    yAxis.Title.Visible = value;
                    sChart.ViewXY.Margins = newMarginsY(sChart.ViewXY.Margins, value);
                }
            }
        }
        public bool xAxisLabelVisible
        {
            get { return xAxis.Title.Visible; }
            set
            {
                if (xAxis.Title.Visible != value)
                {
                    xAxis.Title.Visible = value;
                    sChart.ViewXY.Margins = newMarginsX(sChart.ViewXY.Margins, value);
                }
            }
        }

        private Thickness newMarginsX(Thickness Margins, bool value)
        {
            return new Thickness(Margins.Left, Margins.Top, Margins.Right, Margins.Bottom + ((value) ? 15 : -15));
        }

        private Thickness newMarginsX2(Thickness Margins, bool value)
        {
            return new Thickness(Margins.Left, Margins.Top, Margins.Right, Margins.Bottom + ((value) ? 25 : -25));
        }

        private Thickness newMarginsY(Thickness Margins, bool value)
        {
            return new Thickness(Margins.Left + ((value) ? 20 : -20), Margins.Top, Margins.Right, Margins.Bottom);
        }


        public bool xRangeLabelVisible
        {
            get { return xAxis.LabelsVisible; }
            set
            {
                if (xAxis.LabelsVisible != value)
                {
                    xAxis.LabelsVisible = value;
                    sChart.ViewXY.Margins = newMarginsX2(sChart.ViewXY.Margins, value);
                }
            }
        }


        public double cursorX
        {
            get { return CursorX.ValueAtXAxis; }
            set { CursorX.ValueAtXAxis = value; }
        }

        private Color _TitleColor;
        public Color TitleColor
        {
            get { return _TitleColor; }
            set
            {
                _TitleColor = value;
                xAxis.Title.Color = value;
                yAxis.Title.Color = value;
            }
        }

        private Color _AxisColor;
        public Color AxisColor
        {
            get { return _AxisColor; }
            set
            {
                _AxisColor = value;
                xAxis.AxisColor = value;
                yAxis.AxisColor = value;
                sChart.ViewXY.GraphBorderColor = value;
            }
        }

        private Color _LabelsColor;
        public Color LabelsColor
        {
            get { return _LabelsColor; }
            set
            {
                _LabelsColor = value;
                xAxis.LabelsColor = value;
                yAxis.LabelsColor = value;

                yAxis.MajorDivTickStyle.Color = value;
                yAxis.MinorDivTickStyle.Color = value;
                yAxis.MajorGrid.Color = value;

                xAxis.MajorDivTickStyle.Color = value;
                xAxis.MinorDivTickStyle.Color = value;

                for (int i = 0; i < xAxis.CustomTicks.Count(); i++)
                {
                    xAxis.CustomTicks[i].Color = value;
                }

                xAxis.InvalidateCustomTicks();
            }
        }

        private Color _BackGround;
        public Color BackGround
        {
            get { return _BackGround; }
            set
            {
                _BackGround = value;

                sChart.ChartBackground.Color = value;
            }
        }

        private Color _PointColor;
        public Color PointColor
        {
            get { return _PointColor; }
            set
            {
                _PointColor = value;
                pointStyle.Color1 = Color.FromArgb (50, value.R, value.G , value.B);
            }
        }

        private Color _MarkerTextColor = Colors.White;
        public Color MarkerTextColor
        {
            get { return _MarkerTextColor; }
            set
            {
                _MarkerTextColor = value;

                for (int i = 0; i < FFPLS.SeriesEventMarkers.Count; i++)
                {
                    FFPLS.SeriesEventMarkers[i].Label.Color = value;
                }
            }
        }

        #endregion

        #region Public Metods

        public void ChangeXRange(double startFreq, double endFreq)
        {
            if (!(startFreq < _GlobalRangeXmin || endFreq > GlobalRangeXmax))
                xAxis.SetRange(startFreq, endFreq);
        }

        private void PlotData(int[] dataArray)
        {
            PlotData(dataArray.Select(x => Convert.ToDouble(x)).ToArray());
        }
        private void PlotData(double startFreq, double endFreq, int[] dataArray)
        {
            PlotData(startFreq, endFreq, dataArray.Select(x => Convert.ToDouble(x)).ToArray());
        }
        private void PlotData(double[] dataArray)
        {
            int pointCounter = dataArray.Length;

            var min = sChart.ViewXY.XAxes[0].Minimum;
            var max = sChart.ViewXY.XAxes[0].Maximum;

            var points = new SeriesPoint[pointCounter];
            for (int i = 0; i < pointCounter; i++)
            {
                points[i].X = min + i * (max - min) / pointCounter;
                points[i].Y = dataArray[i];
            }

            FFPLS.Points = points;
        }
        private void PlotData(double startFreq, double endFreq, double[] dataArray)
        {
            int pointCounter = dataArray.Length;

            var points = new SeriesPoint[pointCounter];
            for (int i = 0; i < pointCounter; i++)
            {
                points[i].X = startFreq + i * (endFreq - startFreq) / pointCounter;
                points[i].Y = dataArray[i];
            }

            FFPLS.Points = points;
        }

        private void PlotData(double[] dataArrayX, double[] dataArrayY)
        {
            int pointCounter = dataArrayX.Length;

            var points = new SeriesPoint[pointCounter];

            for (int i = 0; i < pointCounter; i++)
            {
                points[i].X = dataArrayX[i];
                points[i].Y = dataArrayY[i];
            }

            FFPLS.Points = points;
        }

        private struct BearingPoint
        {
            public double[] xdata;
            public double[] ydata;
            public DateTime datetime;

            public BearingPoint(double[] xData, double[] yData, DateTime dateTime)
            {
                xdata = xData;
                ydata = yData;
                datetime = dateTime;
            }
        };

        List<BearingPoint> BearingPoints = new List<BearingPoint>();

        public void BearingPainted(double[] xData, double[] yData)
        {
            FFPLS.Points = new SeriesPoint[0];

            byte Intens = 50;

            pointStyle.Color1 = Color.FromArgb(Intens, pointStyle.Color1.R, pointStyle.Color1.G, pointStyle.Color1.B);

            BearingPoints.Add(new BearingPoint(xData, yData, DateTime.Now));

            for (int i = 0; i < BearingPoints.Count - 1; i++)
            {
                DateTime now = DateTime.Now;
                var diff = now - BearingPoints[i].datetime;
                if (diff.TotalMilliseconds > _LiveTime)
                {
                    BearingPoints.RemoveAt(i);
                }
            }

            double[] plotXData = new double[0];
            double[] plotYData = new double[0];

            for (int i = 0; i < BearingPoints.Count; i++)
            {
                var tempArray = new double[plotXData.Length + BearingPoints[i].xdata.Length];
                plotXData.CopyTo(tempArray, 0);
                BearingPoints[i].xdata.CopyTo(tempArray, plotXData.Length);
                plotXData = tempArray;

                tempArray = new double[plotYData.Length + BearingPoints[i].ydata.Length];
                plotYData.CopyTo(tempArray, 0);
                BearingPoints[i].ydata.CopyTo(tempArray, plotYData.Length);
                plotYData = tempArray;
            }
            PlotData(plotXData, plotYData);
        }

        #endregion

        string _MHz = "МГц";

        #region Special Metods

        //Add marker for each point, to act as an editing point
        private void AddMarkers(SeriesPoint[] points)
        {
            //FFPLS.SeriesEventMarkers.Clear();
            for (int i = 0; i < points.Count(); i++)
            {
                SeriesEventMarker marker = new SeriesEventMarker(FFPLS);
                marker.XValue = points[i].X;
                marker.YValue = points[i].Y;
                //store values in label text 
                //marker.Label.Text = points[i].X.ToString("0.0") + " ; " + points[i].Y.ToString("0.0");
                marker.Label.Text = points[i].X.ToString("0.0000") + " " + _MHz + "\r\n" + points[i].Y.ToString("0.0") + "°";
                //marker.Label.Text = points[i].Amplitude.ToString("0.0000") + " МГц " + "\r\n" + points[i].Angle.ToString("0.0") + "°";
                marker.Label.HorizontalAlign = AlignmentHorizontal.Center;

               //marker.Label.Font = new WpfFont("Segoe UI", 14, true, false);
               //marker.Label.Font = new WpfFont("Segoe UI", 12, false, false);
                marker.Label.Shadow.Style = TextShadowStyle.HighContrast;
                // marker.Label.Shadow.ContrastColor = Colors.Black;
                marker.Label.Color = _MarkerTextColor;
                marker.Label.Shadow.ContrastColor = Colors.Transparent;

                marker.Label.VerticalAlign = AlignmentVertical.Top;

                marker.Label.Visible = true;
                marker.Symbol.GradientFill = GradientFillPoint.Solid;
                //marker.Symbol.Color1 = Colors.Orange;
                marker.Symbol.Color1 = Colors.Transparent;
                marker.Symbol.Width = 5;
                marker.Symbol.Height = 5;
                marker.Symbol.Shape = SeriesMarkerPointShape.Circle;
                marker.VerticalPosition = SeriesEventMarkerVerticalPosition.AtYValue;
                marker.MoveByMouse = false;
                marker.MouseOverOn += new MouseEventHandler(marker_MouseOverOn);
                marker.MouseOverOff += new MouseEventHandler(marker_MouseOverOff);
                marker.Label.Visible = false;

                if (!FFPLS.SeriesEventMarkers.Contains(marker))
                    FFPLS.SeriesEventMarkers.Add(marker);
            }
        }

        private void AddMarkers(SeriesPoint[] points, AlignmentVertical alignmentVertical, AlignmentHorizontal alignmentHorizontal)
        {
            //FFPLS.SeriesEventMarkers.Clear();
            for (int i = 0; i < points.Count(); i++)
            {
                SeriesEventMarker marker = new SeriesEventMarker(FFPLS);
                marker.XValue = points[i].X;
                marker.YValue = points[i].Y;
                //store values in label text 
                //marker.Label.Text = points[i].X.ToString("0.0") + " ; " + points[i].Y.ToString("0.0");
                marker.Label.Text = points[i].X.ToString("0.0000") + " " + _MHz + "\r\n" + points[i].Y.ToString("0.0") + "°";
                //marker.Label.Text = points[i].Amplitude.ToString("0.0000") + " МГц " + "\r\n" + points[i].Angle.ToString("0.0") + "°";
                marker.Label.HorizontalAlign = AlignmentHorizontal.Center;

                //marker.Label.Font = new WpfFont("Segoe UI", 14, true, false);
                //marker.Label.Font = new WpfFont("Segoe UI", 12, false, false);
                marker.Label.Shadow.Style = TextShadowStyle.HighContrast;
                // marker.Label.Shadow.ContrastColor = Colors.Black;
                marker.Label.Color = _MarkerTextColor;
                marker.Label.Shadow.ContrastColor = Colors.Transparent;

                marker.Label.VerticalAlign = alignmentVertical;
                marker.Label.HorizontalAlign = alignmentHorizontal;

                marker.Label.Visible = true;
                marker.Symbol.GradientFill = GradientFillPoint.Solid;
                //marker.Symbol.Color1 = Colors.Orange;
                marker.Symbol.Color1 = Colors.Transparent;
                marker.Symbol.Width = 5;
                marker.Symbol.Height = 5;
                marker.Symbol.Shape = SeriesMarkerPointShape.Circle;
                marker.VerticalPosition = SeriesEventMarkerVerticalPosition.AtYValue;
                marker.MoveByMouse = false;
                marker.MouseOverOn += new MouseEventHandler(marker_MouseOverOn);
                marker.MouseOverOff += new MouseEventHandler(marker_MouseOverOff);
                marker.Label.Visible = false;

                if (!FFPLS.SeriesEventMarkers.Contains(marker))
                    FFPLS.SeriesEventMarkers.Add(marker);
            }
        }

        private void marker_MouseOverOn(object sender, MouseEventArgs e)
        {
            ((SeriesEventMarker)sender).Label.Visible = true;
        }

        private void marker_MouseOverOff(object sender, MouseEventArgs e)
        {
            ((SeriesEventMarker)sender).Label.Visible = false;
            FFPLS.SeriesEventMarkers.Remove(((SeriesEventMarker)sender));
        }

        #endregion

        #region Private Metods

        private void SetOriginScale()
        {
            xAxis.SetRange(_GlobalRangeXmin, _GlobalRangeXmax);
            yAxis.SetRange(_GlobalRangeYmin, _GlobalRangeYmax);
        }

        public void ClearBearing()
        {
            FFPLS.Points = new SeriesPoint[0];
            BearingPoints.Clear();
        }

        private void GenerateCustomTicks(ref AxisX axisX)
        {
            double[] intervals = new double[] { 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1 };

            double interval = axisX.Maximum - axisX.Minimum;
            double intervaldiv = interval / 6;

            var abs = intervals.Select(x => Math.Abs(x - intervaldiv)).ToArray();
            double minvalue = abs.Min();
            int indexmin = Array.IndexOf(abs, minvalue);

            int start = (int)(axisX.Minimum / intervals[indexmin]);

            List<double> dLTickValues = new List<double>();
            dLTickValues.Add(axisX.Minimum);

            var firststep = start * intervals[indexmin];
            while (firststep + intervals[indexmin] < axisX.Maximum)
            {
                firststep = firststep + intervals[indexmin];
                if (Math.Abs(axisX.Minimum - firststep) >= intervals[indexmin] * 0.95)
                    dLTickValues.Add(firststep);
            }
            if (Math.Abs(axisX.Maximum - dLTickValues[dLTickValues.Count() - 1]) >= intervals[indexmin])
                dLTickValues.Add(axisX.Maximum);
            else
                dLTickValues[dLTickValues.Count() - 1] = axisX.Maximum;

            //Set custom ticks for X axis,
            int[] aTickValues = new int[] { 25, 500, 1000, 1500, 2000, 2500, 3025 };

            var dTickValues = dLTickValues.ToArray();

            axisX.CustomTicks.Clear();
            for (int i = 0; i < dTickValues.Length; i++)
            {

                CustomAxisTick cTick = new CustomAxisTick(
                    axisX,
                    dTickValues[i],
                    dTickValues[i].ToString("G5"),
                    6,
                    true,
                    axisX.MajorDivTickStyle.Color,
                    CustomTickStyle.TickAndGrid);

                axisX.CustomTicks.Add(cTick);

            }

            double width = intervals[indexmin];
            double minorwidth = width / 5d;

            List<double> lMinors = new List<double>();

            double first = dTickValues[1];
            double almfirst = dTickValues[0];
            while (almfirst <= first - minorwidth)
            {
                lMinors.Add(first - minorwidth);
                first = first - minorwidth;
            }

            for (int i = 1; i < dTickValues.Count() - 1; i++)
            {
                lMinors.Add(dTickValues[i] + 1 * minorwidth);
                lMinors.Add(dTickValues[i] + 2 * minorwidth);
                lMinors.Add(dTickValues[i] + 3 * minorwidth);
                lMinors.Add(dTickValues[i] + 4 * minorwidth);
            }

            double last = dTickValues[dTickValues.Count() - 1];
            double almlast = dTickValues[dTickValues.Count() - 2];
            while (almlast + minorwidth <= last)
            {
                lMinors.Add(almlast + minorwidth);
                almlast = almlast + minorwidth;
            }

            var minors = lMinors.ToArray();
            Array.Sort(minors);

            for (int i = 0; i < minors.Length; i++)
            {
                CustomAxisTick cTick = new CustomAxisTick(
                    axisX,
                    minors[i],
                    "",
                    3,
                    true,
                    axisX.MinorDivTickStyle.Color,
                    CustomTickStyle.Tick);

                axisX.CustomTicks.Add(cTick);
            }

            //Allow showing the custom tick strings

            axisX.CustomTicksEnabled = true;

            axisX.InvalidateCustomTicks();
        }

        private void _ReCalc()
        {
            _GlobalNumberOfBands = parametrsOfSSR.Count;
            _GlobalRangeXmin = parametrsOfSSR.First().FminMHz;
            _GlobalRangeXmax = parametrsOfSSR.Last().FmaxMHz;
            SetXRange(_GlobalRangeXmin, _GlobalRangeXmax);
        }

        private void SetXRange(double start, double end)
        {
            xAxis.SetRange(start, end);
        }

        private void SetYRange(double start, double end)
        {
            yAxis.SetRange(start, end);
        }

        #endregion

        #region ChartHandlers
        private void xAxis_RangeChanged(object sender, RangeChangedEventArgs e)
        {
            if (e.NewMax - e.NewMin > _GlobalRangeXmax - _GlobalRangeXmin)
            {
                SetOriginScale();
                RangeChangeEvent?.Invoke(xAxis.Minimum, xAxis.Maximum);
                return;
            }

            if (e.NewMax - e.NewMin < 1)
            {
                double center = (xAxis.Maximum + xAxis.Minimum) / 2d;
                xAxis.SetRange(center - 100 / 100d / 2d, center + 100 / 100d / 2d);
            }

            if (e.NewMax > _GlobalRangeXmax)
            {
                double shift = e.NewMax - _GlobalRangeXmax;
                xAxis.SetRange(e.NewMin - shift, e.NewMax - shift);
            }

            if (e.NewMin < _GlobalRangeXmin)
            {
                double shift = _GlobalRangeXmin - e.NewMin;
                xAxis.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
            RangeChangeEvent?.Invoke(xAxis.Minimum, xAxis.Maximum);
            //GenerateCustomTicks(ref xAxis);
        }

        private void yAxis_RangeChanged(object sender, RangeChangedEventArgs e)
        {
            if (e.NewMax - e.NewMin > _GlobalRangeYmax - _GlobalRangeYmin)
            {
                SetOriginScale();
                return;
            }

            if (e.NewMax > _GlobalRangeYmax)
            {
                double shift = e.NewMax - _GlobalRangeYmax;
                yAxis.SetRange(e.NewMin - shift, e.NewMax - shift);
            }

            if (e.NewMin < _GlobalRangeYmin)
            {
                double shift = _GlobalRangeYmin - e.NewMin;
                yAxis.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
        }

        private void sChart_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                var pos = e.GetPosition(sChart);
                var marginsRect = sChart.ViewXY.GetMarginsRect();

                if ((pos.X >= marginsRect.X) && (pos.X <= marginsRect.X + marginsRect.Width) && (pos.Y >= marginsRect.Y) && (pos.Y <= marginsRect.Y + marginsRect.Height))
                {

                    xAxis.CoordToValue((int)pos.X, out var xValue, true);
                    yAxis.CoordToValue((int)pos.Y, out var yValue, true);

                    CursorX.ValueAtXAxis = xValue;

                    CursorOnFreq?.Invoke(xValue);
                }
            }
        }

        private void sChart_MouseMove(object sender, MouseEventArgs e)
        {
            if (FFPLS != null)
            {
                if (FFPLS.Points != null)
                {
                    Point point = e.GetPosition(sChart);
                    if (sChart.ViewXY.FreeformPointLineSeries[0].SolveNearestDataPointByCoord((int)point.X, (int)point.Y, out var xValue, out var yValue, out var nearestIndex))
                    {
                        xAxis.CoordToValue((int)point.X, out var xVal, true);
                        yAxis.CoordToValue((int)point.Y, out var yVal, true);

                        Calc9Zone(xVal, yVal, out var alignmentVertical, out var alignmentHorizontal);

                        SeriesPoint sP = new SeriesPoint(xValue, yValue);
                        //AddMarkers(new SeriesPoint[] { sP });
                        AddMarkers(new SeriesPoint[] { sP }, alignmentVertical, alignmentHorizontal);
                    }
                }
            }
        }

        private void Calc9Zone(double xVal, double yVal, out AlignmentVertical alignmentVertical, out AlignmentHorizontal alignmentHorizontal)
        {
            alignmentVertical = AlignmentVertical.Center;
            alignmentHorizontal = AlignmentHorizontal.Center;

            var xInterval3 = (xAxis.Maximum - xAxis.Minimum) /3d;

            var yInterval3 = (yAxis.Maximum - yAxis.Minimum) / 3d;

            if (xVal < xAxis.Minimum) xVal = xAxis.Minimum;
            if (xVal > xAxis.Maximum) xVal = xAxis.Maximum;
            if (yVal < yAxis.Minimum) yVal = xAxis.Minimum;
            if (yVal > yAxis.Maximum) yVal = xAxis.Maximum;

            if (xVal >= xAxis.Minimum && xVal <= xAxis.Minimum + xInterval3)
            {
                alignmentHorizontal = AlignmentHorizontal.Right;
            }
            if (xVal > xAxis.Minimum + xInterval3 && xVal < xAxis.Minimum + xInterval3 * 2d)
            {
                alignmentHorizontal = AlignmentHorizontal.Center;
            }
            if (xVal >= xAxis.Minimum + xInterval3 * 2d && xVal <= xAxis.Minimum + xInterval3 * 3d)
            {
                alignmentHorizontal = AlignmentHorizontal.Left;
            }

            if (yVal >= yAxis.Minimum && yVal <= yAxis.Minimum + yInterval3)
            {
                alignmentVertical = AlignmentVertical.Top;
            }
            if (yVal > yAxis.Minimum + yInterval3 && yVal < yAxis.Minimum + yInterval3 * 2d)
            {
                alignmentVertical = AlignmentVertical.Center;
            }
            if (yVal >= yAxis.Minimum + yInterval3 * 2d && yVal <= yAxis.Minimum + yInterval3 * 3d)
            {
                alignmentVertical = AlignmentVertical.Bottom;
            }
        }

        #endregion

        #region ReadCreate ConfigSSR
        public void ReadConfigSSR(string bandTableFilename)
        {
            try
            {
                if (parametrsOfSSR.Count > 0)
                {
                    parametrsOfSSR.Clear();
                }

                var options = new JsonSerializerOptions { WriteIndented = true };
                string jsonString = File.ReadAllText(bandTableFilename);
                parametrsOfSSR = JsonSerializer.Deserialize<List<SSRParametrs>>(jsonString, options);
                if (parametrsOfSSR.Count > 0)
                {
                    GlobalRangeXmin = parametrsOfSSR.First().FminMHz;
                    GlobalRangeXmax = parametrsOfSSR.Last().FmaxMHz;
                    GlobalNumberOfBands = (short)parametrsOfSSR.Count;
                }
            }
            catch (Exception e)
            {
                CreateConfigSSR(bandTableFilename);
            }
        }

        private void CreateConfigSSR(string bandTableFilename)
        {
            short Fmin = 30;
            short Bandpass;

            for (int i = 0; i < 79; i++)
            {
                if (i <= 1 || i == 5)
                {
                    Bandpass = 30;
                    parametrsOfSSR.Add(new SSRParametrs((short)i, Fmin, (short)(Fmin + Bandpass)));
                    Fmin += Bandpass;
                }
                else if (i == 2 || i == 38 || i == 53 || i == 78)
                {
                    Bandpass = 40;
                    parametrsOfSSR.Add(new SSRParametrs((short)i, Fmin, (short)(Fmin + Bandpass)));
                    Fmin += Bandpass;
                }
                else if (i <= 4 || (i >= 6 && i <= 11) || (i >= 13 && i <= 37) || (i >= 39 && i <= 52) || (i >= 54 && i <= 64) || (i >= 66 && i <= 77))
                {
                    Bandpass = 80;
                    parametrsOfSSR.Add(new SSRParametrs((short)i, Fmin, (short)(Fmin + Bandpass)));
                    Fmin += Bandpass;
                }
                else if (i == 12 || i == 65)
                {
                    Bandpass = 60;
                    parametrsOfSSR.Add(new SSRParametrs((short)i, Fmin, (short)(Fmin + Bandpass)));
                    Fmin += Bandpass;
                }
            }

            var options = new JsonSerializerOptions { WriteIndented = true };
            string json = JsonSerializer.Serialize(parametrsOfSSR, options);
            File.WriteAllText(bandTableFilename, json);

            if (parametrsOfSSR.Count > 0)
            {
                GlobalNumberOfBands = (short)parametrsOfSSR.Count;
            }
        }
        #endregion

    }
}