﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Arction.Wpf.SemibindableCharting;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Random random = new Random();
        private double [] CreatePointsX()
        {
            int pointCount = 1000;
            double [] points = new double[pointCount];
            for (int i = 0; i < pointCount; i++)
            {
                points[i]= random.NextDouble() * 1000.0;
            }

           return points;
        }

        private double[] CreatePointsY()
        {
            int pointCount = 1000;
            double[] points = new double[pointCount];
            for (int i = 0; i < pointCount; i++)
            {
                points[i] = random.NextDouble() * 360.0;
            }

            return points;
        }

        private void one_Click(object sender, RoutedEventArgs e)
        {
            BControl.BearingPainted(CreatePointsX(), CreatePointsY());
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            BControl.ClearBearing();
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            BControl.TitleColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));

        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            BControl.AxisColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));

        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            BControl.LabelsColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));

        }

        private void six_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            BControl.BackGround = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));

        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            BControl.PointColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            BControl.MarkerTextColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));

        }

        private void nine_Click(object sender, RoutedEventArgs e)
        {
            BControl.GlobalRangeXmin = 100;
        }

        private void ten_Click(object sender, RoutedEventArgs e)
        {
            BControl.SetLanguage("rus");
        }

        private void eleven_Click(object sender, RoutedEventArgs e)
        {
            BControl.SetLanguage("eng");
        }

        private void twelve_Click(object sender, RoutedEventArgs e)
        {
            BControl.SetLanguage("az");
        }
    }
}
